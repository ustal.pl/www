---
title: Ukraina
heading: Kwalifikowane usługi dla ukraińców przebywających na terenie Unii Europejskiej
description: 
---

## Prawo | Закон

[[Tekst w języku ukraińskim] Ustawa Ukrainy z dnia 01.12.2022 nr 2801-IX "O elektronicznej identyfikacji i elektronicznych usługach zaufania"](https://zakon.rada.gov.ua/laws/show/en/2155-19/ed20230101#Text)

[Закон України від 01.12.2022 № 2801-IX "Про електронну ідентифікацію та електронні довірчі послуги"](https://zakon.rada.gov.ua/laws/show/en/2155-19/ed20230101#Text)

- [[EN, PDF] The Law of Ukraine No. 2801-IX of 01.12.2022 "On Electronic Identification and Electronic Trust Services"]({{ site.baseurl }}/ukraina/dokumenty/The_Law_of_Ukraine_No_2801-IX_1_12_2022_On_Electronic_Identification_and_Electronic_Trust_Services.pdf)

Tłumaczenie części ustawy

> Sekcja VII
> PRZEPISY KOŃCOWE I PRZEJŚCIOWE
>
> ...
>
> 6-1. Tymczasowo, do czasu wzajemnego uznania między Ukrainą a Unią Europejską elektronicznych usług zaufania, jako wyjątek od postanowień pierwszej > części artykułu 38 niniejszej ustawy, uznają na Ukrainie:
>
> 1) wyniki otrzymane od kwalifikowanych elektronicznych nadawców elektronicznych usług zaufania, o których informacje oraz o świadczonych przez nich > kwalifikowanych elektronicznych usługach zaufania znajdują się na Trust List państwa członkowskiego Unii Europejskiej lub państwa członkowskiego Europejskiego Stowarzyszenia Wolnego Handlu (dalej – europejscy kwalifikowani dostawcy);
> 
> 2) status tronnich dowirchich usluga zgodnie z niniejszą ustawą;
> 
> 3) status środka kwalifikowanego podpisu elektronicznego lub druku, wykorzystywanego przez europejskich dostawców kwalifikowanych jakościowo przy świadczeniu elektronicznych usług zaufania i znajdującego się na liście certyfikowanych środków składania kwalifikowanego podpisu elektronicznego, prowadzonej przez Komisję Europejską zgodnie z art. 31 Rozporządzenia Parlamentu Europejskiego i Rady (UE) nr 910/2014 z dnia 23 lipca 2014 r. o poczcie elektronicznej. oraz usługi identyfikacji i zaufania dla transakcji elektronicznych na rynku wewnętrznym oraz o uchyleniu dyrektywy 1999/93/FS nadającej status kwalifikowanego podpisu elektronicznego lub druku zgodnie z tą ustawą;
> 
> 4) Komisja zgodnie z art. 22 część czwarta Rozporządzenia Parlamentu Europejskiego i Rady (UE) nr 910/2014 z dnia 23 lipca 2014 r. w sprawie identyfikacji elektronicznej i usług zaufania w transakcjach elektronicznych w ramach rynku wewnętrznego skreślonego dyrektywą 1999/93/FS.
>
> {Rozdział VII „Postanowienia końcowe i przejściowe” dodany do punktów 6-1 zgodnie z ustawą nr 2801-IX z dnia 01.12.2022 r.}


> Розділ VII
> ПРИКІНЦЕВІ ТА ПЕРЕХІДНІ ПОЛОЖЕННЯ
>
> ...
>
> 6-1. Тимчасово, до взаємного визнання між Україною та Європейським Союзом електронних довірчих послуг, як виняток з положень частини першої статті 38 цього Закону, визнати в Україні:
>
> 1) результати надання кваліфікованих електронних довірчих послуг, що надаються кваліфікованими надавачами електронних довірчих послуг, відомості про яких та про кваліфіковані електронні довірчі послуги, які вони надають, внесені до Довірчого списку держави - члена Європейського Союзу або держави - члена Європейської асоціації вільної торгівлі (далі - європейські кваліфіковані надавачі);
> 
> 2) статус європейських кваліфікованих надавачів, що прирівнюється до статусу кваліфікованих надавачів електронних довірчих послуг згідно із цим Законом;
> 
> 3) статус засобів кваліфікованого електронного підпису чи печатки, що використовуються європейськими кваліфікованими надавачами при наданні електронних довірчих послуг та внесені до списку сертифікованих засобів для створення кваліфікованого електронного підпису, який веде Європейська комісія відповідно до статті 31 Регламенту Європейського Парламенту і Ради (ЄС) № 910/2014 від 23 липня 2014 року про електронну ідентифікацію та довірчі послуги для електронних транзакцій на внутрішньому ринку та про скасування Директиви 1999/93/ЄС, що прирівнюється до статусу засобів кваліфікованого електронного підпису чи печатки відповідно до цього Закону;
> 
> 4) перелік довірчих списків держав - членів Європейського Союзу, інформацію про які публікує Європейська Комісія відповідно до частини четвертої статті 22 Регламенту Європейського Парламенту і Ради (ЄС) № 910/2014 від 23 липня 2014 року про електронну ідентифікацію та довірчі послуги для електронних транзакцій на внутрішньому ринку та про скасування Директиви 1999/93/ЄС.
>
>{Розділ VII "Прикінцеві та перехідні положення" доповнено пунктом 6-1 згідно із Законом № 2801-IX від 01.12.2022}

## Linki

- [eSignatures regulations in Ukraine - Adobe.com](https://helpx.adobe.com/legal/esignatures/regulations/ukraine.html)

### Ministerstwo Transformacji Cyfrowej Ukrainy | Міністерство цифрової трансформації України

- [https://thedigital.gov.ua/](https://thedigital.gov.ua/)
- [https://diia.gov.ua/](https://diia.gov.ua/)
- [https://czo.gov.ua](https://czo.gov.ua)
- [https://czo.gov.ua/normative-documentation](https://czo.gov.ua/normative-documentation)

- [[UA] Stosowanie kwalifikowanych i udoskonalonych podpisów elektronicznych](https://czo.gov.ua/edp-legislation-clarification?id=2)
- [Використання кваліфікованих та удосконалених е-підписів](https://czo.gov.ua/edp-legislation-clarification?id=2)
- [Korzystanie z podpisu elektronicznego przez osoby fizyczne-przedsiębiorców](https://czo.gov.ua/edp-legislation-clarification?id=3)
- [Використання е-підпису фізичними особами-підприємцями]((https://czo.gov.ua/edp-legislation-clarification?id=3))
- [Zweryfikuj podpis](https://czo.gov.ua/verify)
- [Перевірити підпис](https://czo.gov.ua/verify)
