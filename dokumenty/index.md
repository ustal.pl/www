---
title: Dokumenty
heading: Zestaw dokumentów
description: Kwalifikowany podpis elektroniczny, kwalifikowana pieczęć elektroniczna, kwalifikowany znacznik czasu
---

## Rozporządzenia

* [ROZPORZĄDZENIE  PARLAMENTU  EUROPEJSKIEGO  I  RADY  (UE)  NR  910/2014 z  dnia  23  lipca  2014  r. w  sprawie  identyfikacji  elektronicznej  i  usług  zaufania  w  odniesieniu  do  transakcji  elektronicznych na  rynku  wewnętrznym  oraz  uchylające  dyrektywę  1999/93/WE]({{ site.baseurl }}/dokumenty/ROZPORZADZENIE_PARLAMENTU_EUROPEJSKIEGO_I_RADY_UE_NR_910_2014.pdf)
