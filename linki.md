---
title: Linki
description: >-
  Baza wiedzy
---

## eID

* [[EN] EU/EEA Trusted List Browser](https://eidas.ec.europa.eu/efda/tl-browser/#/screen/home)
* [[EN] How do I validate an electronic signature or seal as qualified](https://ec.europa.eu/digital-building-blocks/DSS/webapp-demo/validation)
* [[EN] eSiganture](https://ec.europa.eu/digital-building-blocks/wikis/display/DIGITAL/What+is+eSignature)
