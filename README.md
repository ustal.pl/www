# Podpis Kwalifikowany z Pierwszej Stolicy Polski - Gniezno

www.ustal.pl - podpis elektroniczny

## Unia Europejska

* [EN] [Regulacje](https://www.eid.as/#regulation)
* [EN] [Mapa dostawców usług zaufania (Trust service provider)](https://www.eid.as/tsp-map/#/)
* [EN] [eIDAS w 3 minuty](https://go.eid.as/#goals)

## KnowHow

### Podpis kwalifikowany

Kupując certyfikat kwalifikowanego podpisu elektronicznego dysponujesz rozwiązaniem, które załatwia wszystkie problemy nowoczesnego świata: opatrzenie dokumentu kwalifikowanym e-podpisem wywiera skutek prawny równoważny złożeniu podpisu własnoręcznego ([art. 25 ust. 2 rozporządzenia eIDAS](https://eur-lex.europa.eu/legal-content/PL/TXT/?uri=CELEX:32014R0910)), zaś wyrażone w ten sposób oświadczenie woli ma identyczny status jak złożone w formie pisemnej

```
art. 78 kodeksu cywilnego
§ 1. Do zachowania elektronicznej formy czynności prawnej wystarcza złożenie oświadczenia woli w postaci elektronicznej i opatrzenie go kwalifikowanym podpisem elektronicznym.
§ 2. Oświadczenie woli złożone w formie elektronicznej jest równoważne z oświadczeniem woli złożonym w formie pisemnej.
```

### Podpis osobisty (darmowy certyfikat wgrywany przez MSWiA)

```
art. 12d ustawy o dowodach osobistych
1. Opatrzenie danych podpisem osobistym wywołuje w stosunku do podmiotu publicznego skutek prawny równoważny podpisowi własnoręcznemu.
2. Skutek, o którym mowa w ust. 1, wywołuje opatrzenie danych podpisem osobistym w stosunku do podmiotu innego niż podmiot publiczny, jeżeli obie strony wyrażą na to zgodę.
```

### Technicznie

```txt
Rodzaj certyfikatu: Certyfikat kwalifikowany podpisu
Kwalifikowane rozrzerzenie: Certyfikat oznaczony jako certyfikat kwalifikowany: TAK
Format podpisu: PAdES-BASELINE-B
Algorytm podpisu: SHA-256
Algorytm szyfrowania: RSA
Algorytm klucza publicznego: RSA
Długość klucza publicznego: 2048
Wystawca certyfikatu: CUZ Sigillum – QCA1 // Polska Wytwórnia Papierów Wartościowych S.A.

Rodzaj certyfikatu: Certyfikat osobisty (e-dowód)
Kwalifikowane rozrzerzenie: Certyfikat oznaczony jako certyfikat kwalifikowany: NIE
Format podpisu: PAdES-BASELINE-B
Algorytm podpisu: SHA-256
Algorytm szyfrowania: ECDSA
Algorytm klucza publicznego: ECDSA
Długość klucza publicznego: 384
Wystawca certyfikatu: Nazwa powszechna: pl.ID Authorization CA // MSWiA
```

## Artykuły

* [EN] [How eIDAS affects the USA](https://www.cryptomathic.com/news-events/blog/how-eidas-affects-the-us)
* [PL] [Kwalifikowany podpis elektroniczny, podpis osobisty, podpis zaufany… — skutki i moc prawna - 27 marca 2022 przez Olgierd Rudak](https://czasopismo.legeartis.org/2022/03/kwalifikowany-podpis-elektroniczny-podpis-osobisty-zaufany-moc-prawna-skutki-formalne-roznice/)

## Kontakt

* **Paweł Wojciechowski +48509919755**

[Outsourcing IT - Konopnickiej.Com](https://konopnickiej.com)
