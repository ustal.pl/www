---
title: Porównanie Algorytmów Kryptograficznych - AES, DES, 3DES, RSA i ECC
categories:
  - wiedza
  - technicznie
author_staff_member: pawel
date: 2023-07-24
---

## Wprowadzenie

W dzisiejszych czasach, kiedy bezpieczeństwo danych jest kluczowym elementem, wybór odpowiedniego algorytmu kryptograficznego staje się niezwykle istotny. W tym wpisie na bloga dokonamy porównania kilku popularnych algorytmów kryptograficznych, aby lepiej zrozumieć ich zastosowania, charakterystyki oraz poziom bezpieczeństwa.

Omówimy pięć algorytmów: AES, DES, 3DES, RSA i ECC. Zaczniemy od AES, który jest obecnie jednym z najczęściej stosowanych i uznawanych za najbezpieczniejszy algorytm symetryczny. Następnie przeanalizujemy DES i 3DES, które pomimo swojej dawnej popularności, dziś traktowane są jako przestarzałe ze względu na ograniczenia w długości kluczy. Kontynuując, przyjrzymy się algorytmowi RSA, który reprezentuje kryptografię asymetryczną i znajduje szerokie zastosowanie w bezpiecznej komunikacji i wymianie kluczy. Na końcu porównamy algorytm ECC, który dzięki swojej unikatowej naturze matematycznej pozwala na osiągnięcie silnego zabezpieczenia przy użyciu krótszych kluczy, co jest szczególnie korzystne dla urządzeń o ograniczonej mocy obliczeniowej.

Przygotuj się na głębsze zanurzenie w świat kryptografii i odkryj, który z tych algorytmów najlepiej spełni Twoje potrzeby w zależności od konkretnych zastosowań.


## AES (Advanced Encryption Standard)

**Krótki opis:** AES, znany również jako Rijndael, został wybrany przez National Institute of Standards and Technology (NIST) jako zastępstwo dla oryginalnego DES. Jest to obecnie najpopularniejszy i najbezpieczniejszy symetryczny algorytm szyfrowania. AES działa na blokach danych o stałej długości (najczęściej 128 bitów) i jest używany do szyfrowania danych w różnych aplikacjach, takich jak bezpieczna komunikacja, szyfrowanie dysków, VPN i wiele innych.

**Długość klucza:** AES występuje w trzech wariantach, z różnymi długościami klucza - AES-128 (128 bitów), AES-192 (192 bity) i AES-256 (256 bitów).

**Działanie:** AES działa na blokach danych o długości 128 bitów (16 bajtów) i przeprowadza serię rund szyfrowania w zależności od długości klucza. Każda runda składa się z kilku podstawowych operacji, takich jak zastępowanie bajtów, przesunięcia wierszy, mieszanie kolumn i operacje XOR.

**Zastosowania:** AES jest szeroko stosowany do szyfrowania danych w różnych aplikacjach, takich jak bezpieczna komunikacja, szyfrowanie dysków, wirtualne sieci prywatne (VPN), a także w protokołach bezpiecznej wymiany kluczy, takich jak SSL/TLS.

## DES (Data Encryption Standard)

**Krótki opis:** DES był pierwotnie opracowany w latach 70. XX wieku i przez wiele lat był standardem szyfrowania danych. Obecnie uznawany jest za przestarzały ze względu na swoją krótką długość klucza i występowanie znalezionych słabości.

**Długość klucza:** DES używa klucza o długości 56 bitów, co powoduje, że jest podatny na ataki brute force.

**Działanie:** DES działa na blokach danych o długości 64 bitów (8 bajtów) i wykonuje 16 rund operacji, które obejmują permutacje, przesunięcia i przekształcenia S-boksów.

**Zastosowania:** Ze względu na swoje ograniczenia, DES obecnie nie jest rekomendowany do nowych zastosowań. Niemniej jednak, może być nadal stosowany w starszych systemach, które nie wymagają wysokiego poziomu bezpieczeństwa.

## 3DES (Triple Data Encryption Standard)

**Krótki opis:** 3DES jest ulepszoną wersją DES, która miała na celu zwiększenie bezpieczeństwa oryginalnego algorytmu DES poprzez wielokrotne zastosowanie standardowego DES z różnymi kluczami. 

**Długość klucza:** 3DES używa trzech kluczy o długości 56 bitów każdy, co daje łączną długość klucza 168 bitów.

**Działanie:** 3DES wykonuje trzy razy szyfrowanie DES na danych wejściowych, używając kolejno trzech różnych kluczy.

**Zastosowania:** Chociaż 3DES był kiedyś używany jako tymczasowe rozwiązanie, obecnie jego zastosowania są ograniczone ze względu na długi czas działania i długość klucza.

## RSA (Rivest-Shamir-Adleman)

**Krótki opis:** RSA to popularny algorytm kryptograficzny, który działa na zasadzie kryptografii asymetrycznej, używając kluczy publicznych i prywatnych.

**Długość klucza:** RSA wymaga klucza publicznego o długości przynajmniej 2048 bitów, aby zapewnić odpowiedni poziom bezpieczeństwa.

**Działanie:** RSA opiera się na matematycznych właściwościach faktoryzacji liczb dużych liczb pierwszych. Dane szyfrowane są kluczem publicznym, a odszyfrowane kluczem prywatnym.

**Zastosowania:** RSA jest szeroko stosowany w bezpiecznej komunikacji, podpisach cyfrowych (kwalifikowane podpisy elektroniczne korzystają z klucza publicznego o długości 2048 bitów) , a także w protokołach wymiany kluczy, takich jak SSH.

## ECC (Elliptic Curve Cryptography)

**Krótki opis:** ECC jest innym typem algorytmu kryptograficznego, który również działa na zasadzie kryptografii asymetrycznej.

**Długość klucza:** ECC oferuje równie mocne zabezpieczenia co RSA, ale z mniejszymi długościami kluczy. Na przykład klucz ECC o długości 256 bitów zapewnia poziom bezpieczeństwa porównywalny do klucza RSA o długości 3072 bitów.

**Działanie:** ECC wykorzystuje matematyczne właściwości krzywych eliptycznych do generowania kluczy i przeprowadzania operacji kryptograficznych.

**Zastosowania:** ECC jest szczególnie atrakcyjne dla urządzeń o ograniczonej mocy obliczeniowej, takich jak smartfony czy urządzenia IoT, gdzie krótsze klucze pozwalają na osiągnięcie efektywniejszych operacji kryptograficznych. MSWiA wykorzystuje algorytm ECC w 3 certyfikatch które znajdują się na nowym **e-Dowodzie**. Konkretnie używane są klucze publiczne wykorzystujących krzywą eliptyczną **P-384**  w kontekście algorytmu cyfrowego podpisu **ECDSA (Elliptic Curve Digital Signature Algorithm)** oraz algorytmu wymiany klucza **ECDH (Elliptic Curve Diffie-Hellman)** do zapewnienia bezpiecznych podpisów cyfrowych i wymiany kluczy

## Podsumowanie

Podsumowując, AES, DES, 3DES, RSA i ECC to różne rodzaje algorytmów kryptograficznych, które służą różnym celom i mają różne właściwości. AES i ECC są obecnie preferowanymi algorytmami w swoich odpowiednich kategoriach, ze względu na swoją wysoką wydajność i mocne zabezpieczenia. RSA jest nadal szeroko stosowany, ale nieco mniej efektywny od ECC w niektórych zastosowaniach. DES i 3DES, ze względu na swoje ograniczenia, zostały w dużej mierze wyparte przez bardziej zaawansowane algorytmy.