---
title: Podpisy Elektroniczne Wspierają Transformację Cyfrową Ukrainy
categories:
  - prawo
  - ukraina
author_staff_member: pawel
date: 2023-08-09
---

Europa rozpoczyna największy projekt wsparcia transformacji cyfrowej Ukrainy o budżecie całkowitym 17,4 miliona euro

Transformacja cyfrowa to jedno z kluczowych wyzwań współczesnego świata, a Unia Europejska w pełni zdaje sobie z tego sprawę. W ostatnim czasie, Europa rozpoczęła wspieranie Ukrainy w realizacji jej ambitnych celów cyfrowych. Projekt **"Transformacja Cyfrowa dla Ukrainy" (DT4UA)**, z budżetem w wysokości 17,4 miliona euro, ma na celu poprawę efektywności i bezpieczeństwa dostarczania usług publicznych oraz ułatwienie dostępu do nich dla obywateli i przedsiębiorstw w Ukrainie. Projekt ten jest szczególnie istotny w kontekście trwającej wojny oraz procesu odbudowy kraju. W realizacji projektu przoduje Estońska Akademia e-Rządzenia, a od 2016 roku Unia Europejska wsparła Ukrainę w procesie transformacji cyfrowej kwotą przekraczającą 51 milionów euro.

Przedstawicielstwo Unii Europejskiej w Ukrainie podkreśla, że mimo trwającej wojny Ukraina stoi przed jednym z najdynamiczniejszych procesów transformacji cyfrowej na świecie. Jest to osiągnięcie niezwykłe, zwłaszcza w obliczu pełnoskalowej wojny prowadzonej przez Rosję. Unijny projekt DT4UA jest dowodem na gotowość Europy do wsparcia Ukrainy w rozwijaniu usług cyfrowych, które są niezwykle istotne zarówno podczas konfliktu, jak i procesu odbudowy kraju.

Projekt koncentruje się na czterech głównych obszarach:

* Rozwój usług cyfrowych i środowiska dostarczania usług "Diia",
* Udoskonalenie wymiany danych między rejestracjami a dostawcami usług,
* Rozwój infrastruktury tożsamości elektronicznej (eID) zgodnie z regulacją eIDAS,
* Rozwój systemu zarządzania e-Sprawami umożliwiającego bardziej efektywne i transparentne przetwarzanie spraw karnych.
* Projektem beneficjentem jest Ministerstwo Transformacji Cyfrowej Ukrainy, które dąży do stworzenia najbardziej użytkownikowi przyjaznego cyfrowego państwa na świecie.

Jednym z głównych celów jest cyfryzacja 100% usług rządowych. W ciągu trzech lat zdygitalizowano najpopularniejsze i najczęściej wykorzystywane usługi rządowe, a aplikacja "Diya" osiągnęła już poziom międzynarodowy. Współpracując z Unią Europejską, Ukraina pracuje również nad poprawą wymiany danych między rejestrami oraz wzajemnym uznawaniem cyfrowej tożsamości.

To jednak nie koniec wyzwań. Właśnie dlatego Unia Europejska wraz z Estońską Akademią e-Rządzenia, która posiada bogate doświadczenie w współpracy z ukraińskim sektorem publicznym, kontynuuje projekty wspierające rozwój cyfrowy Ukrainy. Współpraca ta obejmuje projekty takie jak EGOV4UKRAINE, EU4DigitalUA i wsparcie dla wzmocnienia cyberbezpieczeństwa. Obecnie Estońska Akademia e-Rządzenia realizuje w Ukrainie cztery projekty o łącznym budżecie 41 milionów euro.

Wnioskując, Estońska Akademia e-Rządzenia wyraża entuzjazm kontynuowania wspólnych działań z Unią Europejską oraz Ministerstwem Transformacji Cyfrowej Ukrainy, aby uczynić Ukrainę jednym z najbardziej zdigitalizowanych społeczeństw na świecie.

## Informacje dodatkowe

Projekt **DT4UA**, wspierany przez Unię Europejską, potrwa od listopada 2022 roku do kwietnia 2025 roku. Jego celem jest poprawa efektywności i bezpieczeństwa dostarczania usług publicznych oraz ułatwienie dostępu do nich dla obywateli i przedsiębiorstw w Ukrainie, zgodnie z wymogami Unii Europejskiej, oraz szybka reakcja na potrzeby spowodowane wojną. Całkowity budżet projektu wynosi 17 400 000 euro. Estońska Akademia e-Rządzenia (EGA) jest wykonawcą projektu. Projekt DT4UA opiera się na osiągnięciach projektów EGOV4UKRAINE i EU4DigitalUA. Od 2016 roku Unia Europejska wsparła transformację cyfrową Ukrainy kwotą przekraczającą 51 milionów euro.
