---
date: 2023-07-13
title: Czym jest kwalifikowany podpis elektroniczny?
categories:
  - wiedza
author_staff_member: pawel
---

Zgodnie z **europejskim ozporządzeniem eIDAS** kwalifikowany podpis elektroniczny to szczególny rodzaj podpisu elektronicznego, który posiada najwyższą moc prawną oraz domniemanie integralności i autentyczności. **Jest odpowiednikiem podpisu odręcznego pod względem uznania prawnego** i może być używany w transakcjach transgranicznych na terenie całej Unii Europejskiej.

Oto kilka kluczowych punktów dotyczących kwalifikowanych podpisów elektronicznych w ramach rozporządzenia eIDAS:

1. **Definicja**: Kwalifikowany podpis elektroniczny jest składany za pomocą kwalifikowanego urządzenia do składania podpisu elektronicznego takiego jak karta inteligentna, bezpieczny token lub **nowe e-Dowody**. W przypadku nowego e-Dowodu znajduje się w nim przestrzeń na zamieszczenie dodatkowych elektronicznych danych (certyfikatu), które umożliwiają składanie takiego podpisu. Certyfikat łączy podpis z osobą podpisującą i potwierdza jej tożsamość.

2. **Moc prawna**: Kwalifikowany podpis elektroniczny jest prawnie równoważny podpisowi odręcznemu i jest dopuszczalny jako dowód w postępowaniu sądowym. Wiąże się z domniemaniem integralności i autentyczności, co oznacza, że zakłada się, że jest autentyczny i niezmieniony, chyba że istnieją dowody przeciwne.

3. **Kwalifikowani dostawcy usług zaufania**: Kwalifikowany podpis elektroniczny musi być wystawiany przez kwalifikowanych dostawców usług zaufania, będących podmiotami spełniającymi określone kryteria określone w Rozporządzeniu eIDAS. Dostawcy ci podlegają rygorystycznym wymogom bezpieczeństwa i operacyjnym, aby zapewnić niezawodność i wiarygodność swoich usług. Nasza firma współpracuje z certyfikowanymi dostawcami celem dostarczania usług kwalifikowanych oraz weryfikowania subskrybentów tych usług.

4. **Uznawanie transgraniczne**: Kwalifikowane podpisy elektroniczne wystawione w jednym państwie członkowskim UE są uznawane i akceptowane w innych państwach członkowskich. Ułatwia to transakcje transgraniczne i eliminuje potrzebę stosowania wielu mechanizmów podpisu w różnych jurysdykcjach w UE. Podpisy kwalifikowane wystawione na terenie Unii Europejskiej uznawane są także w Wielkiej Brytanii oraz [Ukrainie]({{ site.baseurl }}/ukraina/)

5. **Kompatybilność z innymi typami podpisów**: Rozporządzenie eIDAS uznaje różne poziomy podpisów elektronicznych, począwszy od prostych podpisów elektronicznych po zaawansowane podpisy elektroniczne i kwalifikowane podpisy elektroniczne. Chociaż kwalifikowane podpisy elektroniczne zapewniają najwyższy poziom mocy prawnej, rozporządzenie nie unieważnia ani nie ogranicza stosowania innych rodzajów podpisów elektronicznych do określonych celów.

**Kwalifikowane podpisy elektroniczne odgrywają kluczową rolę w zapewnieniu pewności prawnej i bezpieczeństwa transakcji elektronicznych w Unii Europejskiej. Zapewniają zaufany mechanizm dla osób fizycznych i firm do podpisywania dokumentów elektronicznych i uwierzytelniania ich tożsamości w kontekście transgranicznym, promując rozwój usług cyfrowych i handlu elektronicznego w państwach członkowskich UE.**

## Dokumentacja

- [ROZPORZĄDZENIE  PARLAMENTU  EUROPEJSKIEGO  I  RADY  (UE)  NR  910/2014 z  dnia  23  lipca  2014  r. w  sprawie  identyfikacji  elektronicznej  i  usług  zaufania  w  odniesieniu  do  transakcji  elektronicznych na  rynku  wewnętrznym  oraz  uchylające  dyrektywę  1999/93/WE]({{ site.baseurl }}/dokumenty/ROZPORZADZENIE_PARLAMENTU_EUROPEJSKIEGO_I_RADY_UE_NR_910_2014.pdf)
