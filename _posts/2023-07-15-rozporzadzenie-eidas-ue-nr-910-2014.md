---
date: 2023-07-15
title: Rozporządzenie eIDAS - (UE) nr 910/2014
categories:
  - wiedza
author_staff_member: pawel
---

**Rozporządzenie eIDAS**, znane również jako **rozporządzenie (UE) nr 910/2014**, jest rozporządzeniem Unii Europejskiej ustanawiającym ramy dla identyfikacji elektronicznej (eID) i usług zaufania w UE. eIDAS oznacza „elektroniczną identyfikację, uwierzytelnianie i usługi zaufania”.

Podstawowym celem rozporządzenia eIDAS jest stworzenie bezpiecznego i interoperacyjnego środowiska dla transakcji cyfrowych w państwach członkowskich UE. Ma na celu ułatwienie interakcji elektronicznych między osobami fizycznymi, przedsiębiorstwami i administracją publiczną, zapewniając, że tożsamości cyfrowe i podpisy elektroniczne są uznawane i akceptowane ponad granicami.

## Kluczowe przepisy rozporządzenia eIDAS obejmują

1. **Wzajemne uznawanie eID**: Rozporządzenie ustanawia ramy prawne dla wzajemnego uznawania systemów identyfikacji elektronicznej w UE. Umożliwia osobom fizycznym i firmom korzystanie z krajowych identyfikatorów elektronicznych w celu uzyskania dostępu do usług online w innych państwach członkowskich.

2. **Podpisy elektroniczne**: eIDAS definiuje trzy rodzaje podpisów elektronicznych: **proste podpisy elektroniczne**, **zaawansowane podpisy elektroniczne** i [kwalifikowane podpisy elektroniczne]({{ site.baseurl }}/kwalifikowane-uslugi-elektroniczne/). Określa wymagania dotyczące ważności i skutków prawnych tych podpisów, promując ich stosowanie w transakcjach transgranicznych.

3. Usługi zaufania: Rozporządzenie obejmuje różne usługi zaufania, w tym pieczęcie elektroniczne, elektroniczne znaczniki czasu, usługi rejestrowanego doręczenia elektronicznego oraz certyfikaty uwierzytelniania stron internetowych. Określa standardy i wymagania dla tych usług w celu zapewnienia ich bezpieczeństwa i niezawodności.

4. Uznawanie transgraniczne: eIDAS ułatwia transgraniczne uznawanie i akceptację identyfikacji elektronicznej, podpisów elektronicznych i innych usług zaufania. Ma na celu usunięcie barier i promowanie płynnego świadczenia usług cyfrowych w państwach członkowskich UE.

**Rozporządzenie Parlamentu Europejskiego i Rady (UE) nr 910/2014** w sprawie identyfikacji elektronicznej i usług zaufania publicznego w odniesieniu do transakcji elektronicznych na rynku wewnętrznym oraz **uchylające dyrektywę 1999/93/WE, weszło w życie 1 lipca 2016 r**. Stanowi ważne ramy prawne dla promowania zaufania cyfrowego i ułatwiania bezpiecznych transakcji elektronicznych w Unii Europejskiej. Odgrywa kluczową rolę we wspieraniu rozwoju jednolitego rynku cyfrowego i wspieraniu transgranicznych usług cyfrowych.

## Dokumentacja

- [ROZPORZĄDZENIE  PARLAMENTU  EUROPEJSKIEGO  I  RADY  (UE)  NR  910/2014 z  dnia  23  lipca  2014  r. w  sprawie  identyfikacji  elektronicznej  i  usług  zaufania  w  odniesieniu  do  transakcji  elektronicznych na  rynku  wewnętrznym  oraz  uchylające  dyrektywę  1999/93/WE]({{ site.baseurl }}/dokumenty/ROZPORZADZENIE_PARLAMENTU_EUROPEJSKIEGO_I_RADY_UE_NR_910_2014.pdf)
