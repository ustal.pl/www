---
title: Jak zweryfikować podpis kwalifikowany w Adobe Readerze?
categories:
  - wiedza
  - adobe
  - tutorial
author_staff_member:
date: 2023-07-25
---

![Składanie elektronicznego podpisu kwalifikowanego - Ustal.pl]({{ site.baseurl }}/images/posts/skladanie-podpisu.webp)

Gdy otrzymasz [podpis kwalifikowany]({{ site.baseurl}}/kwalifikowane-uslugi-elektroniczne/) zgodnie z rozporządzeniem eIDAS, możesz go zweryfikować w Adobe Readerze (teraz znany jako Adobe Acrobat Reader DC) bez konieczności dodawania dodatkowych plików czy certyfikatów do systemu operacyjnego. Adobe Reader zawiera wbudowane mechanizmy weryfikacji podpisów opartych na certyfikatach kwalifikowanych.

Oto kroki, które należy podjąć, aby zweryfikować podpis kwalifikowany w Adobe Readerze:

- Upewnij się, że masz zainstalowaną **najnowszą wersję Adobe Acrobat Reader DC** na swoim komputerze. Jeśli nie masz tej aplikacji, możesz ją pobrać ze [strony Adobe](https://get.adobe.com/reader/).

- Otwórz dokument PDF, który zawiera podpis kwalifikowany, w Adobe Readerze.

- Znajdź podpis na stronie dokumentu. Podpis kwalifikowany będzie miał pewne cechy odróżniające go od zwykłego podpisu, takie jak specjalny symbol lub dodatkowe informacje o certyfikacie. Nie zawsze jednak musi tak być. To czy będzie on "złożony graficznie" w dokumencie zależy od podpisującego.

[![Poprawnie złożony, graficzny kwalifikowany podpis elektroczniny - Ustal.pl]({{ site.baseurl}}/images/posts/Adobe-poprawny-podpis.webp)]({{ site.baseurl}}/images/posts/Adobe-poprawny-podpis.webp)

- Kliknij lewym przyciskiem myszy na podpisie (lub długo przytrzymaj na urządzeniach dotykowych). Z menu kontekstowego wybierz opcję "Właściwości podpisu".

[![Weryfikacja właściwości kwalifikowanego podpisu elektroczninego - Ustal.pl]({{ site.baseurl}}/images/posts/Adobe-podpis-eutl.webp)]({{ site.baseurl}}/images/posts/Adobe-podpis-eutl.webp)

- W oknie "Właściwości podpisu" kliknij na zakładkę "Pokaż certyfikat autora podpisu...". Tam powinieneś zobaczyć informacje o certyfikacie podpisu, takie jak nazwa wydawcy, numer certyfikatu i informacje o ważności.

- Jeśli podpis został poprawnie zweryfikowany przez Adobe Readera, powinieneś zobaczyć komunikat potwierdzający, że podpis jest prawidłowy, ważny i nie został naruszony.

Warto zaznaczyć, że weryfikacja podpisu kwalifikowanego w Adobe Readerze działa, ponieważ Adobe Reader zawiera zafaną liste certyfikatów EUTL (European Union Trusted List), które są zgodne z regulacjami eIDAS. Oznacza to, że Adobe Reader ma wbudowane certyfikaty, które pozwalają mu sprawdzać autentyczność certyfikatów używanych do podpisywania dokumentów. Nie wymaga to dodatkowych działań ze strony użytkownika ani załadowania dodatkowych plików, ponieważ cała weryfikacja odbywa się wewnątrz aplikacji. 

Gdyby jednak zdażyło się, że Adobe nie pokaże tej informacji należy zaktualizować wewnętrzną listę EUTL programu. W tym celu otwieramy preferencje programu (**Edycja -> Preferencje** lub CTRL + K), wybieramy zakładkę **Mendeżer zaufania** i aktualizujemy listę EUTL klikając na przycisk **Uaktualnij teraz**.

[![Aktualizacja listy certyfikatów EUTL (European Union Trusted List) programu Adobe Reader - Ustal.pl]({{ site.baseurl}}/images/posts/Adobe-PodpisAktualizacjaEUTL.webp)]({{ site.baseurl}}/images/posts/Adobe-PodpisAktualizacjaEUTL.webp)

Po tej procedurze, **autentyczne certyfiakty kwalifikowane** zgodne z regulacjami eIDAS będą prawidłowo rozpoznawane. Możesz wtedy być pewien, że wszystko jest jak należy.

Zawsze upewnij się, że używasz **oficjalnej i zaktualizowanej wersji Adobe Readera** oraz że certyfikaty używane do podpisywania dokumentów są ważne i pochodzą od zaufanego dostawcy usług certyfikacji.
